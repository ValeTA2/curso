$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$('.carousel').carousel({
    			interval:2000
    		});

    	$('#contacto').on('show.bs.modal', function (e){
    		console.log('el modal de contacto se está mostrando');

    		$('#contactoBtn').removeClass('btn-outline-success');
    		$('#contactoBtn').addClass('btn-light');
    		$('#contactoBtn').prop('disabled', true);
    	});

    	$('#contacto').on('shown.bs.modal', function (e){
    		console.log('el modal de contacto se mostró');
    	});

    	$('#contacto').on('hide.bs.modal', function (e){
    		console.log('el modal de contacto se oculta');
    	});

    	$('#contacto').on('hidden.bs.modal', function (e){
    		console.log('el modal de contacto se ocultó');
    		
    		$('#contactoBtn').removeClass('btn-light');
    		$('#contactoBtn').addClass('btn-outline-success');
    		$('#contactoBtn').prop('disabled', false);
    	});

    	});

        //Evento para modo oscuro
        $('#oscuroBtn').on('click', function (){
            if ($('#body').is('.bg-dark')){
                $('#body').removeClass('bg-dark');
            } else {
                    $('#body').addClass('bg-dark');
            }
         });


        

       